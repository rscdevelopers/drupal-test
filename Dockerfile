FROM refstudycentre/drupal-base

###############
# Drupal core #
###############

# https://www.drupal.org/node/3060/release
ENV DRUPAL_VERSION=10.3.2
ENV DRUPAL_MD5=dd37e3aa723db75a04159faed0537eab
RUN set -eux; \
    curl -fSL "https://ftp.drupal.org/files/projects/drupal-${DRUPAL_VERSION}.tar.gz" -o drupal.tar.gz; \
    echo "${DRUPAL_MD5} *drupal.tar.gz" | md5sum -c -; \
    tar -xz --strip-components=1 -f drupal.tar.gz; \
    rm drupal.tar.gz

###############################
# Packages needed for testing #
###############################

# `drupal/core-dev` includes `phpunit` but not `prophecy`.
RUN composer require drupal/core-dev phpspec/prophecy-phpunit:^2

#################
# Configuration #
#################

# Allow dev modules, not just stable.
RUN composer config minimum-stability dev

# Configure PHPUnit
COPY phpunit.xml phpunit.xml

# Add scripts
COPY scripts/* /scripts/
RUN chmod +x /scripts/*

# Set up a location for debugging output
# https://www.drupal.org/docs/testing/phpunit-in-drupal/running-phpunit-tests#debug-output
RUN mkdir -p sites/simpletest/browser_output
RUN chmod -R 777 sites/simpletest/browser_output

# Ensure that the web server can write to these directories
RUN chown -R www-data:www-data sites modules themes

CMD ["/bin/sh"]
