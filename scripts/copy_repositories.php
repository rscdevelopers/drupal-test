#!/usr/bin/env php
<?php

function usage() {
  echo <<<USAGE
Merge the repositories from two composer.json files. 
Usage:

copy_repositories.php FROM TO

Both FROM and TO should be valid `composer.json` files,
although they may have any file names.

FROM is left unchanged, and TO is overwritten.

USAGE;
}

function merge(
  array $source_repositories,
  array $destination_repositories
): array {
  $result = [];

  foreach ($destination_repositories as $repo) {
    if (strpos(json_encode($result), json_encode($repo)) === FALSE) {
      // This is a new repo. Add it.
      $result[] = $repo;
    }
  }

  foreach ($source_repositories as $repo) {
    if (strpos(json_encode($result), json_encode($repo)) === FALSE) {
      // This is a new repo. Add it.
      $result[] = $repo;
    }
  }

  return $result;
}

function copy_repositories(
  string $source_path,
  string $destination_path
): void {
  $source_data = json_decode(file_get_contents($source_path), TRUE);
  $destination_data = json_decode(file_get_contents($destination_path), TRUE);

  $destination_data['repositories'] = merge(
    $source_data['repositories'] ?? [],
    $destination_data['repositories'] ?? []
  );

  file_put_contents($destination_path, json_encode($destination_data, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES));
}

function main($argc, $argv): void {
  $from = $argv[1] ?? NULL;
  $to = $argv[2] ?? NULL;

  if (!$from || !is_string($from) || !$to || !is_string($to)) {
    usage();
    exit(1);
  }

  if (!file_exists($from)) {
    echo "Source file `$from` does not exist.\n";
    exit(2);
  }

  if (!file_exists($to)) {
    echo "Destination file `$to` does not exist.\n";
    exit(2);
  }

  copy_repositories($from, $to);
}

main($argc, $argv);
