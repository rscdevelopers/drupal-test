# drupal-test docker image

For testing private Drupal modules using PHPUnit and Bitbucket Pipelines

## Rationale

Bitbucket allows you to use services, but you can't share volumes between your 
main image (into which your repository is cloned) and the service containers. 
To run a Drupal module's functional tests, you need `php-fpm`, `nginx` and 
`PHPUnit` all in the same container. This is what this image provides.

This image does not contain the database, since that can easily be provided 
using a service in Bitbucket pipelines.

## Suggested pipeline configuration

See `bitbucket-pipelines.example.yml` for recommended Bitbucket Pipelines 
configuration for a custom Drupal module.

## Useful scripts inside the container

- `/scripts/serve.sh` starts the Nginx and PHP-FPM daemons
- `/scripts/copy_repositories.sh` merges the repositories from two `composer.json` files. 

## How to ...

### Build and run locally

```shell
docker build -t refstudycentre/drupal-test:latest .
```

```shell
docker run -p 80:80 --rm --name drupal-test refstudycentre/drupal-test:latest
```

### Update on hub.docker.com

See [bitbucket-pipelines.yml](bitbucket-pipelines.yml)
